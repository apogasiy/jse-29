package com.tsc.apogasiy.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsc.apogasiy.tm.api.entity.IWBS;
import com.tsc.apogasiy.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @Nullable
    private Date startDate = null;

    @Nullable
    private Date finishDate = null;

    @Nullable
    private Date created = new Date();

    public Task(@NotNull final String userId, @NotNull final String name) {
        this.userId = userId;
        this.name = name;
    }

    public Task(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        setUserId(userId);
        setName(name);
        setDescription(description);
    }

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name;
    }

}
