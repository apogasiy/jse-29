package com.tsc.apogasiy.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsc.apogasiy.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String hashedPassword;

    @Nullable
    private String email;

    @Nullable
    private String lastName = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String middleName = "";

    @Nullable
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    public User(@NotNull final String login, @NotNull String hashedPassword) {
        this.login = login;
        this.hashedPassword = hashedPassword;
    }

}
