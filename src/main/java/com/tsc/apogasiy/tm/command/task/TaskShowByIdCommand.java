package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import com.tsc.apogasiy.tm.model.Task;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-show-by-id";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Show task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
