package com.tsc.apogasiy.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileOutputStream;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-save-fasterxml-yaml";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to yaml by FasterXML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(getDomain());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(serviceLocator.getPropertyService().getDTOYamlFileName());
        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.close();
    }

}
