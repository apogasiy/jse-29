package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-create";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Create task";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
    }

}
