package com.tsc.apogasiy.tm.command.system;

import com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class DisplayAllCommand extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "commands";
    }

    @Override
    public @Nullable String getArgument() {
        return "-cmd";
    }

    @Override
    public @NotNull String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands)
            System.out.println(command.getCommand());
    }

}
