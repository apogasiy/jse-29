package com.tsc.apogasiy.tm.service;

import com.tsc.apogasiy.tm.api.service.IPropertyService;
import com.tsc.apogasiy.tm.exception.system.UnknownPropertyException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Optional;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PSWD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PSWD_ITER_KEY = "password.iteration";

    @NotNull
    private static final String APP_VER_KEY = "application.version";

    @NotNull
    private static final String DEV_NAME_KEY = "developer.name";

    @NotNull
    private static final String DEV_EMAIL_KEY = "developer.email";

    @NotNull
    private static final String DTO_FILE_BIN = "dto.file.bin";

    @NotNull
    private static final String DTO_FILE_BASE64 = "dto.file.base64";

    @NotNull
    private static final String DTO_FILE_XML = "dto.file.xml";

    @NotNull
    private static final String DTO_FILE_JSON = "dto.file.json";

    @NotNull
    private static final String DTO_FILE_YAML = "dto.file.yaml";

    @NotNull
    private static final String AUTOSAVE_INTERVAL = "autosave.interval.sec";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private final HashMap<String, String> propertyDefaultMap = new HashMap<>();

    {
        propertyDefaultMap.put(PSWD_SECRET_KEY, "730333210");
        propertyDefaultMap.put(PSWD_ITER_KEY, "16553");
        propertyDefaultMap.put(APP_VER_KEY, "1.0");
        propertyDefaultMap.put(DEV_NAME_KEY, "Ivan Petrov");
        propertyDefaultMap.put(DEV_EMAIL_KEY, "ipetrov@mailbox.su");
        propertyDefaultMap.put(DTO_FILE_BIN, "./data.bin.def");
        propertyDefaultMap.put(DTO_FILE_BASE64, "./data.base64.def");
        propertyDefaultMap.put(DTO_FILE_XML, "./data.xml.def");
        propertyDefaultMap.put(DTO_FILE_JSON, "./data.json.def");
        propertyDefaultMap.put(DTO_FILE_YAML, "./data.yaml.def");
        propertyDefaultMap.put(AUTOSAVE_INTERVAL, "10");
    }

    @SneakyThrows(IOException.class)
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (Optional.ofNullable(inputStream).isPresent()) {
            properties.load(inputStream);
            inputStream.close();
        }
    }

    @NotNull
    private String getPropertyValue(@NotNull final String name) {
        if (!propertyDefaultMap.containsKey(name))
            throw new UnknownPropertyException();
        if (System.getProperties().containsKey(name))
            return System.getProperty(name);
        if (System.getenv().containsKey(name))
            return System.getenv(name);
        return properties.getProperty(name, propertyDefaultMap.get(name));
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return getPropertyValue(APP_VER_KEY);
    }

    @Override
    @NotNull
    public String getDeveloperEmail() {
        return getPropertyValue(DEV_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getDeveloperName() {
        return getPropertyValue(DEV_NAME_KEY);
    }

    @Override
    @NotNull
    public String getDTOBase64FileName() {
        return getPropertyValue(DTO_FILE_BASE64);
    }

    @Override
    @NotNull
    public String getDTOBinFileName() {
        return getPropertyValue(DTO_FILE_BIN);
    }

    @Override
    @NotNull
    public String getDTOXmlFileName() {
        return getPropertyValue(DTO_FILE_XML);
    }

    @Override
    @NotNull
    public String getDTOJsonFileName() {
        return getPropertyValue(DTO_FILE_JSON);
    }

    @Override
    @NotNull
    public String getDTOYamlFileName() {
        return getPropertyValue(DTO_FILE_YAML);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getPropertyValue(PSWD_SECRET_KEY);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return Integer.parseInt(getPropertyValue(PSWD_ITER_KEY));
    }

    @Override
    @NotNull
    public Integer getAutosaveInterval() {
        return Integer.parseInt(getPropertyValue(AUTOSAVE_INTERVAL));
    }

}
