package com.tsc.apogasiy.tm.service;

import com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import com.tsc.apogasiy.tm.api.service.IProjectService;
import com.tsc.apogasiy.tm.enumerated.Status;
import com.tsc.apogasiy.tm.exception.empty.*;
import com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import com.tsc.apogasiy.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @Nullable
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        if (description == null || description.isEmpty())
            throw new EmptyDescriptionException();
        @NotNull final Project project = new Project(userId, name, description);
        projectRepository.add(project);
    }

    @Override
    @Nullable
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    private void update(@NotNull final Project project, @NotNull final String name, @Nullable final String description) {
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    @Nullable
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        @NotNull final Project project = projectRepository.findById(id);
        if (Optional.ofNullable(project).isPresent())
            update(project, name, description);
        return project;
    }

    @Override
    @Nullable
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        @NotNull final Project project = projectRepository.findByIndex(index);
        if (Optional.ofNullable(project).isPresent())
            update(project, name, description);
        return project;
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return projectRepository.existsByIndex(index);
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @NotNull final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return projectRepository.existsByName(userId, name);
    }

    @Override
    @Nullable
    public Project startById(@Nullable final String userId, @Nullable final String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @Override
    @Nullable
    public Project startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return projectRepository.startByIndex(userId, index);
    }

    @Override
    @Nullable
    public Project startByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @Override
    @Nullable
    public Project finishById(@Nullable final String userId, final @Nullable String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @Override
    @Nullable
    public Project finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return projectRepository.finishByIndex(userId, index);
    }

    @Override
    @Nullable
    public Project finishByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @Override
    @Nullable
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return projectRepository.changeStatusById(userId, id, status);
    }

    @Override
    @Nullable
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return projectRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    @Nullable
    public Project changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return projectRepository.changeStatusByName(userId, name, status);
    }

}
