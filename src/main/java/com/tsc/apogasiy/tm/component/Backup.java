package com.tsc.apogasiy.tm.component;

import com.tsc.apogasiy.tm.command.data.DataBackupCommand;
import com.tsc.apogasiy.tm.command.data.DataLoadCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService;

    public Backup(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.executorService = Executors.newSingleThreadExecutor();
    }

    public void start() {
        executorService.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(bootstrap.getPropertyService().getAutosaveInterval() * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                save();
            }
        });
    }

    @SneakyThrows
    public void load() {
        bootstrap.runCommand(DataLoadCommand.NAME, false);
    }

    @SneakyThrows
    public void save() {
        DataBackupCommand command = new DataBackupCommand();
        bootstrap.runCommand(DataBackupCommand.NAME, false);
    }

}
